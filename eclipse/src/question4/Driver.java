package question4;

import java.util.ArrayList;
import java.util.Collection;

import question3.Planet;

public class Driver {
	public static void main(String[] args) {
		Planet mercury = new Planet("milk","mercury",1,50);
		Planet mercury2 = new Planet("milk","mercury",1,25);
		Planet mercury3 = new Planet("milk","mercury",1,20);
		Planet venus = new Planet("milk","venus",2,60);
		Planet mars = new Planet("milk","mars",4,40);
		
		//Creating Collection of planets 
		Collection<Planet> planets = new ArrayList<Planet>();
		planets.add(mercury);
		planets.add(mercury2);
		planets.add(mercury3);
		planets.add(venus);
		planets.add(mars);
		
		//Calling the method 
		Collection<Planet> planetsLargeThan20 = CollectionsMethods.getLargerThan(planets,26);
		//Printing the new Collection of planets
		for(Planet p : planetsLargeThan20) {
			System.out.println(p.getName()+" "+p.getRadius());
		}
	}
}
