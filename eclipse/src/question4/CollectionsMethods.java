package question4;
import java.util.ArrayList;
import java.util.Collection;

import question3.Planet;

public class CollectionsMethods {
	
	/**
	 * Returns a collection with planets with a radius bigger or equal to the size specified 
	 * @param planets Collection of planets 
	 * @param size Radius which only planets with radius larger than this will be added to the collection
	 * @return Collection with planets with radius larger or equal to the size specified
	 */
	public static Collection<Planet> getLargerThan(Collection<Planet> planets, double size){
		Collection<Planet> planetsLargerThanSize = new ArrayList<Planet>();
		//Loops through all the planets
		for (Planet p : planets) {
			//If the radius of the planet is larger than the size, it will add it to the collection
			if (p.getRadius() >= size) {
				planetsLargerThanSize.add(p);
			}
		}
		//Returns Collection with only planets larger than the size 
		return planetsLargerThanSize;
	}
}
