package question3;

/**
 * A class to store information about a planet
 * @author Daniel
 *
 */
public class Planet implements Comparable<Planet> {
	/**
	 * The name of the solar system the planet is contained within
	 */
	private String planetarySystemName;
	
	/**
	 * The name of the planet.
	 */
	private String name;
	
	/**
	 * From inside to outside, what order is the planet. (e.g. Mercury = 1, Venus = 2, etc)
	 */
	private int order;
	
	/**
	 * The size of the radius in Kilometers.
	 */
	private double radius;

	/**
	 * A constructor that initializes the Planet object
	 * @param planetarySystemName The name of the system that the planet is a part of
	 * @param name The name of the planet
	 * @param order The order from inside to out of how close to the center the planet is
	 * @param radius The radius of the planet in kilometers
	 */
	public Planet(String planetarySystemName, String name, int order, double radius) {
		this.planetarySystemName = planetarySystemName;
		this.name = name;
		this.order = order;
		this.radius = radius;
	}
	
	/**
	 * @return The name of the planetary system (e.g. "the solar system")
	 */
	public String getPlanetarySystemName() {
		return planetarySystemName;
	}
	
	/**
	 * @return The name of the planet (e.g. Earth or Venus)
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @return The rank of the planetary system
	 */
	public int getOrder() {
		return order;
	}


	/**
	 * @return The radius of the planet in question.
	 */
	public double getRadius() {
		return radius;
	}
	
	/**
	 * Overriding the equals method, Planets are equal when they have the same name and planetary system name
	 * @author Le Duytam
	 */
	@Override
	public boolean equals(Object o) {
		//If the object is not an instance of a Planet, returns false immediatly 
		if(! (o instanceof Planet)) {
			return false;
		}
		
		//Casting the object to a Planet
		Planet other = (Planet)o;
		
		//Checks if the name current object and the other object are the same 
		boolean isSameName = this.name.equals(other.name);
		
		//Checks if the Planetary System Name of the current object is the same for the other object
		boolean isSamePlaneteraySystemName = this.planetarySystemName.equals(other.planetarySystemName);
		
		//Returns true if they are both the same
		return isSameName && isSamePlaneteraySystemName;
		
	}
	
	/**
	 * Overriding the hashCode method, Consistent with the equals method 
	 * Should be the same hash code if the name and planetary system name are the same
	 * @author Le Duytam
	 */
	@Override
	public int hashCode() {
		String mergedString = this.name+this.planetarySystemName;
		return mergedString.hashCode();
	}

	/**
	 * Overriding the compareTo method, it will look at the names first(increasing order), if they are the same, it will look at the the radius (decreasing order)
	 * @author Le Duytam
	 */
	@Override
	public int compareTo(Planet o) {
		int nameComparisson = this.name.compareTo(o.name);
		//if it is the same name, it will then look at the radius
		if(nameComparisson == 0 ) {
			return (int)(o.radius - this.radius);
		}
		//Otherwise, it will only look at the name
		return nameComparisson;
	}
	
	
}
