package question3;

import java.util.Arrays;

public class Driver {
	public static void main(String[] args) {
		Planet mercury = new Planet("milk","mercury",1,50);
		Planet mercury2 = new Planet("milk","mercury",1,25);
		Planet mercury3 = new Planet("milk","mercury",1,20);
		Planet venus = new Planet("milk","venus",2,60);
		Planet mars = new Planet("milk","mars",4,40);
		
		//Checks if the equals works
		boolean isEquals = mercury.equals(mercury2);
		System.out.println(isEquals);
		
		//Checks if hashCod is the same 
		boolean isHashCodeSame = mercury.hashCode() == mercury2.hashCode();
		System.out.println(isHashCodeSame);
		
		//Creating the planets array, and adding planets 
		Planet[] planets = {mercury,mercury2,mercury3,venus,mars};
		//Sorts the array
		Arrays.sort(planets);
		//Prints the sorted array
		for (Planet p : planets) {
			System.out.println(p.getName()+" "+p.getRadius());
		}
		//Mars,Mercury1,Mercury2,Mercury3,Venus
	}
}
