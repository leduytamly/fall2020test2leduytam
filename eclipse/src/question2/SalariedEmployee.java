package question2;

public class SalariedEmployee implements Employee {
	private double yearlySalary;
	
	/**
	 * Constructor for SalariedEmployee
	 * @param yearlySalary The pay for the year worked 
	 */
	public SalariedEmployee(int yearlySalary) {
		if(yearlySalary < 0) {
			throw new IllegalArgumentException("Yearly salary cannot be negative");
		}
		this.yearlySalary = yearlySalary;
	}
	
	/**
	 * Getter for yearlySalary
	 * @return The yearly salary
	 */
	public double getYearlySalary() {
		return this.yearlySalary;
	}
	
	/**
	 * Overriding the getWeeklyPlay method, returns the yearlySalary divided by 52 weeks 
	 */
	@Override
	public double getWeeklyPay() {
		return this.yearlySalary/52;
	}

}
