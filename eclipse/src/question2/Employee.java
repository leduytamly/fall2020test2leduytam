package question2;

public interface Employee {
	/**
	 * Computes the weekly pay
	 * @return the weekly pay in dollars
	 */
	public double getWeeklyPay();
}
