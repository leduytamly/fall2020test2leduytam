package question2;

public class PayrollManagement {
	public static void main(String[] args) {
		//Creating employees
		SalariedEmployee alice = new SalariedEmployee(30000);
		HourlyEmployee bob = new HourlyEmployee(40,20);
		UnionizedHourlyEmployee charles = new UnionizedHourlyEmployee(45,10,30,1.5);
		HourlyEmployee ethan = new HourlyEmployee(30,20);
		UnionizedHourlyEmployee felix = new UnionizedHourlyEmployee(30,14,35,1.2);
		
		//Creating the Employee array and adding the employees to the array
		Employee[] emps = new Employee[5];
		emps[0] = alice;
		emps[1] = bob;
		emps[2] = charles;
		emps[3] = ethan;
		emps[4] = felix;
		
		//Computing the total expense and printing it
		System.out.println(getTotalExpenses(emps));
	}
	
	/**
	 * Computes the total salary of all employees in an array, rounds to 2 decimal 
	 * @param emps
	 * @return the total expense 
	 */
	public static double getTotalExpenses(Employee[] emps) {
		double sum = 0;
		for (Employee e : emps) {
			sum+= e.getWeeklyPay();
		}
		//Returns the total expense rounded to 2 decimals
		return Math.round(sum*100.0)/100.0;
	}
	
}
