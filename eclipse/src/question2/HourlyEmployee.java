package question2;

public class HourlyEmployee implements Employee{
	private int numberHoursPerWeek;
	private double hourlyPay;
	
	/**
	 * Constructor for HourlyEmployee
	 * @param numberHoursPerWeek The number of hours that the employee worked for in a week
	 * @param hourlyPay The pay per hour worked
	 */
	public HourlyEmployee(int numberHoursPerWeek,double hourlyPay) {
		if(numberHoursPerWeek < 0) {
			throw new IllegalArgumentException("Number of hours cannot be negative");
		}
		if (hourlyPay < 0 ) {
			throw new IllegalArgumentException("Hourly pay cannot be negative");
		}
		this.numberHoursPerWeek = numberHoursPerWeek;
		this.hourlyPay = hourlyPay;
	}
	
	/**
	 * Getter for numberHoursPerWeek
	 * @return The number of hours that the employee worked for in a week
	 */
	public int getNumberHoursPerWeek() {
		return this.numberHoursPerWeek;
	}
	
	/**
	 * Getter for hourlyPay
	 * @return The pay per hour worked
	 */
	public double getHourlyPay() {
		return this.hourlyPay;
	}
	
	/**
	 * Overriding the getWeeklyPay method, multiplies the number of hours worked per week by the hourly pay
	 */
	@Override
	public double getWeeklyPay() {
		return numberHoursPerWeek * hourlyPay;
	}
	
}
