package question2;

public class UnionizedHourlyEmployee extends HourlyEmployee{
	private int maxHoursPerWeek;
	private double overtimeRate;
	
	/**
	 * Constructor for UnionizedhourlyEmployee
	 * @param numberHoursPerWeek The number of hours that the employee worked for in a week
	 * @param hourlyPay The pay per hour worked
	 * @param maxHoursPerWeek Maximum number of hours per week
	 * @param ovetimeRate The overtime rate 
	 */
	public UnionizedHourlyEmployee(int numberHoursPerWeek, double hourlyPay, int maxHoursPerWeek, double ovetimeRate) {
		super(numberHoursPerWeek, hourlyPay);
		if(maxHoursPerWeek < 0) {
			throw new IllegalArgumentException("Maximum number of hours cannot be negative");
		}
		if(overtimeRate < 0) {
			throw new IllegalArgumentException("Overtime rate cannot be negative");
		}
		
		this.maxHoursPerWeek = maxHoursPerWeek;
		this.overtimeRate = ovetimeRate;
	}
	
	/**
	 * Overriding the getWeeklyPay method
	 * If the number of hours per week is less then the maximum hours per week, multiplies the hourlypay with the number of hours per week
	 * If the number of hours per week is more than the maxium hours, the pay is hourly pay times the number of hours plus the hourly pay times the number of extra hours times the overtime rate 
	 */
	@Override
	public double getWeeklyPay() {
		//When number of hours is less or equal to the maximum hours per week
		if (super.getNumberHoursPerWeek() <= this.maxHoursPerWeek) {
			//Calculates pay as usual 
			return super.getWeeklyPay();
			
		} else {
			//Calculates the time difference between number of hours and maximum hours per week
			int timeDifference = super.getNumberHoursPerWeek() - maxHoursPerWeek;
			
			//Calculates the pay with overtime
			double payWithOvertime = (super.getHourlyPay() * overtimeRate * timeDifference);
			
			//Calculates the pay before overtime
			double payBeforeOvertime = (super.getNumberHoursPerWeek() - timeDifference)*super.getHourlyPay();
			
			//Returns the sum of the two
			return payWithOvertime + payBeforeOvertime;
		}
	}
	
}
